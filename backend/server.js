const express = require("express");
const graphqlHTTP = require("express-graphql");
const { buildSchema } = require("graphql");
const axios = require("axios").default;
const cors = require("cors");

const client = axios.create({
  baseURL: "https://api.github.com/graphql",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
    Authorization: "bearer cc0d3dc7e47742888f67fbeb5c431aa8289e771f",
  },
});

const schema = buildSchema(`
  type Issue {
    title: String
    number: Int
  }

  type Author {
    login: String
    avatarUrl: String
    url: String
  }

  type IssueComment {
    bodyText: String
    author: Author
  }

  type IssueDetail {
    title: String
    number: Int
    author: Author
    comments: [IssueComment]
  }

  type Query {
    getIssues: [Issue]
    getIssueDetail(number: Int!): IssueDetail
  }
`);

const root = {
  getIssues: async () => {
    const data = await client.post("", {
      query: `
        query issues {
          repository(owner: "facebook", name: "react") {
            issues(first:20) {
              edges {
                node {
                  title
                  number          
                }
              }
            }
          }
        }
        `,
    });
    const issues = data.data.data.repository.issues.edges;

    return issues.map(issue => ({
      title: issue.node.title,
      number: issue.node.number,
    }));
  },

  getIssueDetail: async ({ number }) => {
    const res = await client.post("", {
      query: `
        query {
          repository(owner: "facebook", name: "react") {
            issue(number: ${number}){
              title
              number
              author {
                login
                avatarUrl
                url
              }
              comments(first: 10) {
                nodes {
                  bodyText
                  author {
                    login
                    avatarUrl
                    url
                  }
                }
              }
            }
          }
        }
        `,
    });
    const { comments, ...issue } = res.data.data.repository.issue;
    const formattedComment = comments.nodes;
    return {
      ...issue,
      comments: formattedComment,
    };
  },
};

const app = express();
app.use(cors());

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
);

app.listen(8080, () =>
  console.log("Running GraphQL API server on localhost:8080/graphql")
);
