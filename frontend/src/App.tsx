import * as React from 'react';
import { hot } from 'react-hot-loader/root';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// import pages here
import Index from './pages/index';
import IssueDetail from './pages/IssueDetail';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" render={props => <Index {...props} />} />
        <Route
          exact
          path="/detail"
          render={props => <IssueDetail {...props} />}
        />
      </Switch>
    </BrowserRouter>
  );
}

export default hot(App);
