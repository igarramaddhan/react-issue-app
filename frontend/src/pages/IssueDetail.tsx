import React from 'react';

import { RouteComponentProps } from 'react-router';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import qs from 'querystring';

const GET_ISSUES = gql`
  query issue($number: Int!) {
    getIssueDetail(number: $number) {
      title
      number
      author {
        login
        avatarUrl
        url
      }
      comments {
        bodyText
        author {
          login
        }
      }
    }
  }
`;

interface IssueDetailQuery {
  number: string;
}

const IssueDetail: React.FC<RouteComponentProps> = props => {
  const search = (qs.decode(
    props.location.search.slice(1)
  ) as unknown) as IssueDetailQuery;
  if (!search.number) {
    console.log('nooo');
    return <h1>Oops, something went wrong!</h1>;
  }
  return (
    <div className="container">
      <h2 className="page-title">Facebook Github Issues</h2>
      <Query query={GET_ISSUES} variables={{ number: parseInt(search.number) }}>
        {({ loading, error, data }) => {
          if (loading) return <p className="loading">Loading...</p>;
          if (error) return <p className="error">Error! {error.message}</p>;
          const { title, number, author, comments } = data.getIssueDetail;
          return (
            <React.Fragment>
              <p className="issue-title">
                <span style={{ color: '#30edff' }}>#{number}</span> {title}
              </p>
              <span className="issue-author">by: {author.login}</span>
              <h4 className="issue-comment-label">Comments</h4>
              <ul>
                {comments.length != 0 ? (
                  comments.map((comment, index) => (
                    <li key={index} className="comment-container">
                      <p style={{ color: '#ff9b30' }}>{comment.author.login}</p>
                      <p style={{ fontSize: '18px' }}>{comment.bodyText}</p>
                    </li>
                  ))
                ) : (
                  <p style={{ color: '#b9bdc4' }}>No comment</p>
                )}
              </ul>
            </React.Fragment>
          );
        }}
      </Query>
    </div>
  );
};

export default IssueDetail;
