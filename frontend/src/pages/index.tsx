import * as React from 'react';
import '../style/index.scss';

import { RouteComponentProps } from 'react-router';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import qs from 'querystring';

interface Props {}

const GET_ISSUES = gql`
  {
    getIssues {
      title
      number
    }
  }
`;

const Index: React.FC<Props & RouteComponentProps> = props => {
  return (
    <div className="container">
      <h2 className="page-title">Facebook Github Issues</h2>
      <Query query={GET_ISSUES}>
        {({ loading, error, data }) => {
          if (loading) return <p className="loading">Loading...</p>;
          if (error) return <p className="error">Error! {error.message}</p>;
          return (
            <ul>
              {data.getIssues.map(issue => (
                <li
                  key={issue.number}
                  className="comment-container"
                  onClick={() =>
                    // props.history.push('/detail', { number: issue.number })
                    props.history.push({
                      pathname: '/detail',
                      search: qs.encode({ number: issue.number }),
                    })
                  }
                  style={{
                    cursor: 'pointer',
                    // fontSize: '18px',
                    // margin: '8px 0',
                  }}
                >
                  <p style={{ fontSize: '18px' }}>
                    <span style={{ color: '#30edff', fontWeight: 'bold' }}>
                      #{issue.number}
                    </span>{' '}
                    {issue.title}
                  </p>
                </li>
              ))}
            </ul>
          );
        }}
      </Query>
    </div>
  );
};

export default Index;
